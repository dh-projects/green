__author__ = "Alix Chagué"
__copyright__ = "2024 Alix Chagué"
__credits__ = ["Alix Chagué"]
__license__ = "MIT License"
__version__ = "0.0.1"

"""Apply Hugo Scheithauer's script to the IAM database to blend realistic backgrounds on each image."""

import os
import sys

from tqdm import tqdm

# check if the folder containing the fake background exists
path_to_backgrounds = os.path.join("..", "..", "Gallicalbum")

if not os.path.isdir(path_to_backgrounds):
    print(f"Please download the Gallicalbum dataset and correct '{path_to_backgrounds}' in make_green_iam.py")
    sys.exit()
else:
    path_to_backgrounds = os.path.join(path_to_backgrounds, "data") # Warning: value of path_to_backgrounds has changed!
    backgrounds = [f for f in os.listdir(path_to_backgrounds) if f.lower().endswith('.jpg') or f.lower().endswith('.png') or f.lower().endswith('.jpeg')]
    if len(backgrounds) == 0:
        print(f"Please download the Gallicalbum dataset and correct 'path_to_backgrounds' in make_green_iam.py. The folder seems to be empty ({path_to_backgrounds}).")
        sys.exit()

# check if the folder containing the IAM dataset exists
path_to_iam_pairs = "../get_iam/iam_pairs_rgba/"

if not os.path.isdir(path_to_iam_pairs):
    print("Please download the IAM dataset and correct 'path_to_iam_pairs' in make_green_iam.py")
    sys.exit()
else:
    iam_pairs = [f for f in os.listdir(path_to_iam_pairs) if os.path.isdir(os.path.join(path_to_iam_pairs, f))]
    if len(iam_pairs) == 0:
        print(f"Please download the IAM dataset and correct 'path_to_iam_pairs' in make_green_iam.py. The folder seems to be empty ({path_to_iam_pairs}).")
        sys.exit()

# check if the folders containing the subsets of the IAM dataset exist
subsets = [] 
iam_pairs_folders = [f for f in os.listdir(path_to_iam_pairs) if os.path.isdir(os.path.join(path_to_iam_pairs, f))]
for iam_pairs_folder in iam_pairs_folders:
    iam_pairs_folder_path = os.path.join(path_to_iam_pairs, iam_pairs_folder)
    if iam_pairs_folder not in ["trainset", "testset", "validationset1", "validationset2"]:
        print(f"[I] {iam_pairs_folder_path} will be skipped.")
    else:
        iam_pairs_folder_content = os.listdir(iam_pairs_folder_path)
        if len(iam_pairs_folder_content) == 0:
            print(f"Please check the folder {iam_pairs_folder_path}. It seems to be empty.")
            print("Skipping this folder.")
        else:
            subsets.append(iam_pairs_folder_path)

print("[I] The following subsets will be blended: ", subsets)


# for each folder in ../get_iam/iam_pairs, copy the folder and its content to green/iam_pairs_blended
if not os.path.isdir("iam_pairs_blended"):
    os.mkdir("iam_pairs_blended")

for subset in tqdm(subsets, desc="Blending the subsets of the IAM dataset"):
    subset_name = os.path.basename(subset)
    subset_blended = os.path.join("iam_pairs_blended", subset_name)

    if not os.path.isdir(subset_blended):
        os.mkdir(subset_blended)
    
    for file in tqdm(os.listdir(subset), desc="Copying the txt files from the subset"):
        if file.endswith(".txt"):
            os.system(f"cp {os.path.join(subset, file)} {os.path.join(subset_blended, file)}")
    
    print("Blending the images from the subset: ", subset_name)
    os.system(f"python3 blending.py -i {subset} -bg {path_to_backgrounds} -o {subset_blended}")

    # Sanity check
    print("Sanity check on the data from the subset: ", subset_name)
    n_txt_files = len([f for f in os.listdir(subset) if f.lower().endswith('.txt')])
    n_img_files = len([f for f in os.listdir(subset) if f.lower().endswith('.png') or f.lower().endswith('.jpg') or f.lower().endswith('.jpeg')])
    n_txt_files_blended = len([f for f in os.listdir(subset_blended) if f.lower().endswith('.txt')])
    n_img_files_blended = len([f for f in os.listdir(subset_blended) if f.lower().endswith('.png') or f.lower().endswith('.jpg') or f.lower().endswith('.jpeg')])
    if n_txt_files != n_txt_files_blended:
        print(f"[W] {n_txt_files - n_txt_files_blended} txt files were not copied.")
    else:
        print(f"[I] {n_txt_files} txt files were successfully copied.")
    if n_img_files != n_img_files_blended:
        print(f"[W] {n_img_files - n_img_files_blended} img files were not copied.")
    else:
        print(f"[I] {n_img_files} img files were successfully copied.")
    print(f"[W] {n_txt_files} txt files and {n_img_files} img files were found in the original subset.")
    print(f"[I] {n_txt_files_blended} txt files and {n_img_files_blended} img files were found in the blended subset.")


print("Done blending IAM-DB with real backgrounds.")
