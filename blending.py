import os, random

import cv2 as cv
from PIL import Image
import numpy as np
from blend_modes import darken_only


def read_image(path):
    """Load an image with opencv"""
    # this .astype syntax causes errors...
    try:
        return cv.imread(path, -1).astype(float)
    except Exception as e:
        print(f"Error while loading {path}: {e}")
        print("Trying another syntax, with PIL...")
    img = Image.open(path)
    return np.array(img).astype(float)
    


def select_background(background_files):
    """Select a random background from the backgrounds folder and load it with opencv"""
    random_background = random.choice(background_files)
    background_img = read_image(random_background)
    return background_img


def crop_bg(background_img, sample_x, sample_y):
    """Randomly crops a background based on the dimension of another image"""
    # chose a random x and y coordinate in a range that will fit the sample img (max_len - sample_len)
    try:
        x = np.random.randint(0, background_img.shape[1] - sample_x)
        y = np.random.randint(0, background_img.shape[0] - sample_y)
    except ValueError:
        # muting this message because it shouldn't impact us down the line anymore
        #print("ValueError: sample image is larger than the background image")
        return "Error"
    
    # crop the background img and convert it to BGRA   
    crop = background_img[y: y + sample_y, x: x + sample_x].astype(np.uint8)
    rgba = cv.cvtColor(np.asarray(crop), cv.COLOR_BGR2BGRA).astype(float)
    return rgba


def is_compatible(sample, background_shape):
    """Checks if the sample and background are compatible"""
    if sample.shape[1] < background_shape[1] or sample.shape[0] < background_shape[0]:
        return True
    return False


def main(args):
    """Main function"""

    possible_background_files = os.listdir(args.background_dir)
    if len(possible_background_files) == 0:
        print(f"The folder {args.background_dir} is empty!")
        exit()
    possible_background_files = [os.path.join(args.background_dir, f) for f in possible_background_files if f.lower().endswith('.jpg') or f.lower().endswith('.png') or f.lower().endswith('.jpeg')]
    print(f"Found {len(possible_background_files)} possible backgrounds for blending the images.")

    # let's not try to blend txt files (or other non image files) with a background ;)
    acceptable_extensions = ["jpg", "png", "jpeg"]
    processable_files = [f for f in os.listdir(args.sample_dir) if f.split(".")[-1].lower() in acceptable_extensions]

    skipped_files = []
    for file in processable_files:
        print("Processing: ", os.path.join(args.sample_dir, file))
        sample_img_path = os.path.join(args.sample_dir, file)
        if not os.path.isfile(sample_img_path):
            print(f"Skipping: {file} because the path leads to an invalid file.")
            skipped_files.append(file)
            continue
        try:
            sample_img = read_image(sample_img_path)
        except Exception as e:
            print(f"Skipping: {file} because of {e}")
            skipped_files.append(file)
            continue

        # select a random background, try several times before giving up
        background_img = select_background(possible_background_files)
        try: # this try/except is here in case img.shape raises an error
            cropped_bg = crop_bg(background_img, sample_img.shape[1], sample_img.shape[0])
        except Exception as e:
            cropped_bg = "Error"

        max_try=100
        while (not is_compatible(sample_img, background_img.shape) or isinstance(cropped_bg, str)) and max_try > 0:
            background_img = select_background(possible_background_files)
            try: # again, this try/except is here in case img.shape raises an error
                cropped_bg = crop_bg(background_img, sample_img.shape[1], sample_img.shape[0])
            except Exception as e:
                cropped_bg = "Error"
            max_try -= 1
        if max_try == 0:
            print(f"Skipping: {file} because no compatible background could be found.")
            print(f"either {file} is too big. You need to resize the background or the sample.")
            print(f"or cropped_bg is somehow always an instance of string (?).")
            skipped_files.append(file)
            continue

        if isinstance(cropped_bg, str):
            print(f"Skipping: {file}. But the previous condition in the loop should have prevented this from happening.")
            skipped_files.append(file)
            continue

        if cropped_bg.shape != sample_img.shape:
            print("[W] The cropped background and the sample image have different shapes. This might cause problems.")
        blended_img_float = darken_only(sample_img, cropped_bg, 1)
        blended_img_uint8 = blended_img_float.astype(np.uint8)

        cv.imwrite(os.path.join(args.output_dir, file), blended_img_uint8)

    print("Done blending.")
    print(f"[I] {len(skipped_files)} files were skipped.")
    print(f"[I] So {len(processable_files) - len(skipped_files)} files were succefully processed.")
    print("[I] The skipped files are: ", skipped_files)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--sample_dir", default="./handwriting_samples",
                        help="Path to the folder containing the images to be blended with the backgrounds.")
    parser.add_argument("-bg", "--background_dir", default="./backgrounds",
                        help="Path to the folder containing the backgrounds.")
    parser.add_argument("-o", "--output_dir", default="./blended_images",
                        help="Path to the folder where the blended images will be saved.")
    args = parser.parse_args()

    # checking the value of the arguments
    if not os.path.isdir(args.sample_dir):
        print("The sample directory does not exist.")
        exit()
    else:
        print(f"The images to be blended are in: {args.sample_dir}")
    if not os.path.isdir(args.background_dir):
        print("The background directory does not exist.")
        exit()
    else:
        print(f"The available backgrounds are in: {args.background_dir}")
    if not os.path.isdir(args.output_dir):
        print("The output directory does not exist. Creating it...")
        os.mkdir(args.output_dir)
        print("Created: ", args.output_dir)
    else:
        print(f"The blended images will be saved in: {args.output_dir}")
    
    main(args)
