__author__ = "Alix Chagué"
__copyright__ = "2024 Alix Chagué"
__credits__ = ["Alix Chagué"]
__license__ = "MIT License"
__version__ = "0.0.1"

"""Takes folders of images and change their mode from grayscale to RGBA"""

import os
import sys
from PIL import Image

from tqdm import tqdm

PATH_OUTPUT = "../get_iam/iam_pairs_rgba"
PATH_TO_IAM_PAIRS = "../get_iam/iam_pairs/"

# check if the folder containing the IAM dataset exists
if not os.path.isdir(PATH_TO_IAM_PAIRS):
    print("Please download the IAM dataset and correct 'PATH_TO_IAM_PAIRS' in make_green_iam.py")
    sys.exit()
else:
    iam_pairs = [f for f in os.listdir(PATH_TO_IAM_PAIRS) if os.path.isdir(os.path.join(PATH_TO_IAM_PAIRS, f))]
    if len(iam_pairs) == 0:
        print(f"Please download the IAM dataset and correct 'PATH_TO_IAM_PAIRS' in make_green_iam.py. The folder seems to be empty ({PATH_TO_IAM_PAIRS}).")
        sys.exit()

# check if the folders containing the subsets of the IAM dataset exist
subsets = [] 
iam_pairs_folders = [f for f in os.listdir(PATH_TO_IAM_PAIRS) if os.path.isdir(os.path.join(PATH_TO_IAM_PAIRS, f))]
for iam_pairs_folder in iam_pairs_folders:
    iam_pairs_folder_path = os.path.join(PATH_TO_IAM_PAIRS, iam_pairs_folder)
    if iam_pairs_folder not in ["trainset", "testset", "validationset1", "validationset2", "left_out"]:
        print(f"[I] {iam_pairs_folder_path} will be skipped.")
    else:
        iam_pairs_folder_content = os.listdir(iam_pairs_folder_path)
        if len(iam_pairs_folder_content) == 0:
            print(f"Please check the folder {iam_pairs_folder_path}. It seems to be empty.")
            print("Skipping this folder.")
        else:
            subsets.append(iam_pairs_folder_path)

print("[I] The following subsets will be processed: ", subsets)


# for each folder in ../get_iam/iam_pairs, copy the folder and its content to PATH_OUTPUT
if not os.path.isdir(PATH_OUTPUT):
    os.mkdir(PATH_OUTPUT)

for subset in tqdm(subsets, desc="Creating the RGBA subsets for the IAM dataset"):
    subset_name = os.path.basename(subset)
    subset_rgbaed = os.path.join(PATH_OUTPUT, subset_name)

    if not os.path.isdir(subset_rgbaed):
        os.mkdir(subset_rgbaed)
    
    for file in tqdm(os.listdir(subset), desc="Copying the txt files from the subset or converting the images to RGBA"):
        if file.endswith(".txt"):
            os.system(f"cp {os.path.join(subset, file)} {os.path.join(subset_rgbaed, file)}")
        else:
            img = Image.open(os.path.join(subset, file))
            img_rgba = img.convert("RGBA")
            img_rgba.save(os.path.join(subset_rgbaed, file))

    # Sanity check
    print("Sanity check on the data from the subset: ", subset_name)
    n_txt_files = len([f for f in os.listdir(subset) if f.lower().endswith('.txt')])
    n_img_files = len([f for f in os.listdir(subset) if f.lower().endswith('.png') or f.lower().endswith('.jpg') or f.lower().endswith('.jpeg')])
    n_txt_files_rgbaed = len([f for f in os.listdir(subset_rgbaed) if f.lower().endswith('.txt')])
    n_img_files_rgbaed = len([f for f in os.listdir(subset_rgbaed) if f.lower().endswith('.png') or f.lower().endswith('.jpg') or f.lower().endswith('.jpeg')])
    if n_txt_files != n_txt_files_rgbaed:
        print(f"[W] {n_txt_files - n_txt_files_rgbaed} txt files were not copied.")
    else:
        print(f"[I] {n_txt_files} txt files were successfully copied.")
    if n_img_files != n_img_files_rgbaed:
        print(f"[W] {n_img_files - n_img_files_rgbaed} img files were not copied.")
    else:
        print(f"[I] {n_img_files} img files were successfully copied.")
    print(f"[I] {n_txt_files} txt files and {n_img_files} img files were found in the original subset.")
    print(f"[I] {n_txt_files_rgbaed} txt files and {n_img_files_rgbaed} img files were found in the blended subset.")


print("Done changing the mode of the images (Grayscale to RGBA).")

